
#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h> /* для mtrace */
#include <string.h>
#define MAX_LEN 1024
/*4
Расположить строки в обратном алфавитном порядке 
Входные параметры:
1. Массив 
2. Размерность массива 
Выходные параметры:
1. Количество перестановок
2. Длина первой строки 
* 
* ЗАДАНИЕ:
Написать программу сортировки массива строк по вариантам.
Строки вводить с клавиатуры. Сначала пользователь вводит кол-во строк потом сами строки. 
Для тестирования использовать перенаправление ввода-вывода ($./myprog < test.txt). 
Память для строк выделять динамически с помощью функций malloc или calloc.
Ввод данных, сортировку и вывод результатов оформить в виде функций.
Сортировку делать с помощью qsort если возможно, если нет то писать свой вариант сортировки.
Входные и выходные параметры функции сортировки указаны в варианте. 
Входные и выходные параметры функций для ввода-вывода:
*
* Прототип функции для ввода строк 
length = inp_str(char* string, int maxlen); 
// length – длина строки 
// string – введенная строка 
// maxlen – максимально возможная длина строки (размерность массива string) 

Прототип функции для вывода строк 
void out_str(char* string, int length, int number);
// string – выводимая строка 
// length – длина строки 
// number – номер строки 

Модифицировать программу, применив в функциях передачу параметров и возврат результатов по ссылке (с использованием указателей).
Сравнить результаты. 
*/

int inp_str(char* string, int maxlen)
{
		char buffer[MAX_LEN];
		char bufflen[MAX_LEN];
		sprintf(bufflen, " %%%d[^\n]s", maxlen);
		scanf(bufflen, buffer);
		//getchar();
		//fflush(stdin);
		//printf("Выпилена строка: %s\n", buffer);
		strcpy(string, buffer);
		size_t len = strlen(buffer);
		scanf("%[^\r\n]", buffer);
		return (int) len;
} 

void out_str(char* string, int length, int number)
{
	char bufflen[MAX_LEN];
	sprintf(bufflen, "%%d %%%ds\n", length);
	printf(bufflen, number+1, string);
};

int bubbleSort(char **mas, int count);

int* resultFunction(char ** mas, int count)
{
	int *a = (int*)malloc(sizeof(int)*2);
	a[0] = bubbleSort(mas, count);
	a[1] = (int) strlen(mas[0]);
	//Расположить строки в обратном алфавитном порядке
	return a;
}

int bubbleSort(char **mas, int count)
{
	int numberSwap = 0;
	for(int i = 0; i < count; i++)
	{
		for(int j = 0; j < count; j++)
		{
			if(strcmp(mas[i], mas[j]) > 0)
			{				
				char *swap;
				swap = mas[i];
				mas[i] = mas[j];
				mas[j] = swap;
				numberSwap++;
			}
		}
	}		
	return numberSwap;
}

void freeMas(char **mas, int count){
	printf("\n");
	for (int i = count - 1; i >= 0 ; i--){		
        free(mas[i]); // освобождаем память для отдельной строки
    }
    free(mas); // освобождаем памать для массива указателей на строки
}
/*
void printMas(char **mas, int count)
{
	for(int i = 0; i < count; i++)
	{
		printf("%s\n",mas[i]);
	}
}
*/
void printMas(char **mas, int count)
{
	for(int i = 0; i < count; i++)
	{
		out_str(mas[i], 20, i);
	}
}

char** readMas(char** mas, int count, int len)
{
	for(int i = 0; i < count; i++)
	{
		mas[i] = (char *)malloc(sizeof(char)*len);
		inp_str(mas[i], 20);
	}	
	return mas;
}
/*
char** readMas(int count){
	char buffer[MAX_LEN];
	char **mas;  //указатель на массив указателей на строки
	mas = (char **)malloc(sizeof(char *)*count);// выделяем память для массива указателей
    for (int i = 0; i < count ; i++)
    {
        printf("Введите строку:\n");
		scanf(" %[^\n]", &buffer);
		//printf("strlen = %d", (int) strlen(buffer));
        mas[i] = (char *)malloc(sizeof(char)*(strlen(buffer)+1)); //выделяем память для строки
        strcpy(mas[i], buffer); //копируем строку из буфера в массив указателей
    }
    return mas; 
}
*/
int main(int argc, char *argv[])
{
	char **mas = NULL;
	int count = 0 ;
	mtrace();
	printf("Введите количество строк:\n");
	scanf("%d%*c", &count);
	//getchar();
	mas = (char **)malloc(sizeof(char*)*count);
	mas = readMas(mas, count, MAX_LEN);
	
	printMas(mas, count);
	
	int* result = NULL;
	result = resultFunction(mas, count);
	printf("\n");
	printMas(mas, count);
	freeMas(mas, count);
	printf("Результат: \nКоличество перестановок: %d \nДлина первой строки: %d\n", result[0], result[1]); 
	free(result);
	
	return 0;
}

